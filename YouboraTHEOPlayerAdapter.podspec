Pod::Spec.new do |s|

  s.name         = 'YouboraTHEOPlayerAdapter'
  s.version      = '6.7.1'

  # Metadata
  s.summary      = 'Adapter to use YouboraLib on TheoPlayer'

  s.description  = <<-DESC
                      YouboraTHEOPlayerAdapter is an adapter used for TheoPlayer.
                      DESC

  s.homepage     = 'http://developer.nicepeopleatwork.com/'

  s.license      = { :type => 'MIT', :file => 'LICENSE.md' }

  s.author             = { 'NPAW' => 'support@nicepeopleatwork.com' }

  # Platforms
  s.ios.deployment_target = '11.0'
  s.tvos.deployment_target = '11.0'

  # Platforms
  s.swift_version = '4.0', '4.1', '4.2', '4.3', '5.0', '5.1', '5.2', '5.3'

  # Source Location
  s.source       = { :git => 'https://bitbucket.org/npaw/theoplayer-adapter-ios.git', :tag => s.version }
	
  # Source files
  s.source_files = 'YouboraTHEOPlayerAdapter tvOS/**/*.swift'

  # Project settings
  s.pod_target_xcconfig = {
    'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) YOUBORATHEOPLAYERADAPTER_VERSION_SETTING=' + s.version.to_s,
  }

  # Dependency
  s.dependency 'YouboraLib', '~> 6.5'

end
