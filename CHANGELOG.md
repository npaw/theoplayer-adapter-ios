## [6.7.1] - 2024-05-15
### Changed
- Changed adapter rendition getters according to THEOPlayer v6.x
- Changed ads adapter playhead getter according to THEOPlayer v6.x

## [6.7.0] - 2023-01-12
### Added
- `fireSeekEnd` on seeked event
- `fireBufferEnd` on `CanPlayThrough` event

### Fixed
- Wrong references to waiting and ended event listeners

### Removed
- Playhead monitor

## [6.6.7] - 2022-06-14
### Changed
- TheoPlayer listener references to weak

## [6.6.6] - 2022-04-28
### Removed
- TheoPlayer network listener as a workaround for the player not being able to unregister its events

## [6.6.5] - 2022-04-26
### Changed
- TheoPlayer network instance condition when player is destroyed

## [6.6.4] - 2022-04-25
### Added
- TheoPlayer destroy event

## [6.6.3] - 2022-02-16
### Removed
- TheoPlayer podspec dependency

## [6.6.2] - 2022-01-13
### Fixed
- Buffer detection on iOS 15

## [6.6.1] - 2021-11-17
### Added
- TheoPlayer podspec dependency as optional

## [6.6.0] - 2021-11-03
### Fixed
- Support TheoPlayer with cocoapods

## [6.5.0] - 2021-07-08
### Added
- Support to TheoPlayer cocoapods version >= 2.82.0
- Added ad join when start request is called

## [6.0.9] - 2020-10-23
### Added
- Version of the player to the string to be sent with the version

## [6.0.8] - 2020-10-21
### Added
- Support to TheoPlayer version >= 2.76.0

### Removed
- Google cast as dependency from pods

## [6.0.7] - 2020-10-19
### Added
- Adapter for the new ads
- Support to TheoPlayer version >= 2.70.0

### Removed
- Google cast as dependency from pods

## [6.0.6] - 2020-06-22
### Added
- Google cast as dependency  

## [6.0.5] - 2020-05-26
### Added
- Support to THEOPlayer version >= 2.69.0

### Changed
- Distinguish between fatal and not fatal error

## [6.0.4] - 2020-04-29
### Fixed
- Version to be sent by the adapter

## [6.0.3] - 2020-04-22
### Removed
- Remove google cast from the dependencies

## [6.0.2] - 2020-03-25
### Changed
- Update TheoPlayer to v6.67
- Add fireStop when change source
- Trigger fireSeek just once
- Improve the way like the player gets the version
- Add Cocoapods and Carthage support

## [6.0.1] - 2020-01-14
### Fixed
- Rendition unwrapping nil values

## [6.0.0] - 2018-10-26
### Added
- Release version
