//
//  PlayerViewController.swift
//  THEOPlayerExampleiOS
//
//  Created by nice on 13/01/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

import Foundation
import UIKit
import THEOplayerSDK
import YouboraLib
import YouboraConfigUtils
import THEOplayerGoogleIMAIntegration

class PlayerViewController: UIViewController {
    
    var viewModel: PlayerViewModel?
    
    var theoPlayer: THEOplayer?
    
    @IBOutlet var playerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let options = YouboraConfigManager.getOptions()
        options.contentResource = viewModel?.resourceUrl
        
        self.viewModel?.setPlugin(options: options)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupPlayer()
        self.setupAdapters()
        self.theoPlayer?.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isBeingDismissed || self.isMovingFromParent {
            self.viewModel?.stopYoubora()
        }
    }
    
    
    func setupPlayer() {
        guard let viewModel = self.viewModel else {
            return
        }

        let playerConfig = THEOplayerConfiguration(license: "your_license_here")
        let theoplayer = THEOplayer(configuration: playerConfig)
        
        let imaIntegration = GoogleIMAIntegrationFactory.createIntegration(on: theoplayer)
        theoplayer.addIntegration(imaIntegration)
        
        
        theoplayer.frame = playerView.bounds
        theoplayer.addAsSubview(of: self.playerView)
        
        theoplayer.fullscreenOrientationCoupling = true

        theoplayer.source = viewModel.playerSource
        
        self.theoPlayer =  theoplayer
    }
    
    func setupAdapters() {
        guard let viewModel = self.viewModel else {
            return
        }
        
        viewModel.setupAdapter(player: self.theoPlayer)
        
        if (viewModel.ads.count > 0) {
            viewModel.setupAdsAdapter(player: self.theoPlayer)
        }
    }
    
    @IBAction func onChangeItemPress(sender: UIButton) {
        theoPlayer?.source = viewModel?.getOtherSource()
    }
}
