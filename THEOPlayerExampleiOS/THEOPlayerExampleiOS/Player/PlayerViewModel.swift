//
//  PlayerViewModel.swift
//  THEOPlayerExampleiOS
//
//  Created by nice on 13/01/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

import Foundation
import THEOplayerSDK
import YouboraLib
import YouboraConfigUtils
import YouboraTHEOPlayerAdapter

class PlayerViewModel {
    
    let resourceUrl: String
    
    var typedSource: TypedSource {
        return TypedSource(src: self.resourceUrl, type: "application/x-mpegurl")
    }
    
    var ads:[GoogleImaAdDescription] = []
    
    var playerSource: SourceDescription {
        return SourceDescription(source: typedSource, ads: self.ads, poster: "https://cdn.theoplayer.com/video/elephants-dream/playlist.png")
    }
    var plugin: YBPlugin?
    
    init(resourceUrl: String, includeAds: Bool) {
        self.resourceUrl = resourceUrl
        
        if includeAds {
            ads.append(GoogleImaAdDescription(src: "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostpod&cmsid=496&vid=short_onecue&correlator="
            ))
        }
    }
    
    func setPlugin(options: YBOptions) {
        self.plugin = YBPlugin(options: options)
        YBLog.setDebugLevel(.debug)
    }
    
    func setupAdapter(player: THEOplayer?) {
        guard let player = player else {
            return
        }
        self.plugin?.fireInit()
        let adapter = YBTHEOPlayerAdapter(player: player)
        adapter.appendNetworkFatalErros(networkFatalErros: [404, 403])
        
        self.plugin?.adapter = adapter
    }
    
    func setupAdsAdapter(player: THEOplayer?) {
        guard let player = player else {
            return
        }
        self.plugin?.adsAdapter = YBTHEOPlayerAdsAdapter(player: player)
    }
    
    func getOtherSource() -> SourceDescription {
        let randomSource = Int.random(in: 0..<resources.count)
        let typeSourceTmp =  TypedSource(src: resources[randomSource], type: "application/x-mpegurl")
        return SourceDescription(source: typeSourceTmp, ads: self.ads, poster: "https://cdn.theoplayer.com/video/elephants-dream/playlist.png")
    }
    
    func stopYoubora() {
        self.plugin?.fireStop()
    }
}
