//
//  MenuViewModel.swift
//  THEOPlayerExampleiOS
//
//  Created by nice on 13/01/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

import Foundation

class MenuViewModel {
    var currentResource = RESOURCE_URL
    var includeAds = false
    
    func updateResource(newResource: String?) {
        guard let newResource = newResource else {
            return
        }
        self.currentResource = newResource
    }
    
    func updateAds(includeAds: Bool) {
        self.includeAds = includeAds
    }
    
    func buildPlayerViewModel() -> PlayerViewModel {
        return PlayerViewModel(resourceUrl: self.currentResource, includeAds: self.includeAds)
    }
    
}
