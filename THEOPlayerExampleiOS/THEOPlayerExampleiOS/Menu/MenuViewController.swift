//
//  MenuViewController.swift
//  THEOPlayerExampleiOS
//
//  Created by nice on 13/01/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

import UIKit
import YouboraConfigUtils

class MenuViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var resourceTextField: UITextField!
    @IBOutlet weak var adsSwitch: UISwitch!
    
    let viewModel = MenuViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resourceTextField.delegate = self

        self.setInitialValues()
        // Do any additional setup after loading the view.
    }
    
    func setInitialValues() {
        self.resourceTextField.text = viewModel.currentResource
        self.adsSwitch.setOn(viewModel.includeAds, animated: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    @IBAction func onTextFieldDidChange(_ sender: UITextField) {
        if sender == self.resourceTextField {
            self.viewModel.updateResource(newResource: sender.text)
        }
    }
    
    @IBAction func onSwitchDidChange(_ sender: UISwitch) {
        if sender == self.adsSwitch {
            self.viewModel.updateAds(includeAds: sender.isOn)
        }
    }
}

// MARK: - Navigation
extension MenuViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let playerViewController = segue.destination as? PlayerViewController {
            playerViewController.viewModel = self.viewModel.buildPlayerViewModel()
            return
        }
    }
    
    @IBAction func navigateToSettings(_ sender: Any) {
        guard let navigationController = self.navigationController else {
            return
        }
        
        navigationController.show(YouboraConfigViewController.initFromXIB(), sender: nil)
    }
}
