//
//  Utils.swift
//  YouboraTHEOPlayerAdapter
//
//  Created by Tiago Pereira on 09/03/2020.
//  Copyright © 2020 Enrique Alfonso Burillo. All rights reserved.
//

import UIKit
#if canImport(THEOplayerSDK)
import THEOplayerSDK

public struct Utils {

    static let PLAYER_NAME = "TheoPlayer"
    static let ADAPTER_STRING_VERSION = "6.7.1"
    
    public static func getOsName() -> String {
        #if os(iOS)
            return "iOS"
        #elseif os(tvOS)
            return "tvOS"
        #else
            return "unknown"
        #endif
    }
    
    public static func getPlayerVersion() -> String {
        return "\(PLAYER_NAME)-\(THEOplayer.version)"
    }
    
    public static func getPlayerName(ads: Bool) -> String {
        if ads {
            return "\(PLAYER_NAME)-Ads"
        }
        
        return PLAYER_NAME
    }
    
    public static func getPlayerNameWithOs(ads: Bool) -> String {
       return "\(getPlayerName(ads: ads))-\(getOsName())"
    }
    
    public static func getPlayerNameWithOsAndVersion(ads: Bool) -> String {
          return "\(ADAPTER_STRING_VERSION)-\(getPlayerNameWithOs(ads: ads))"
       }
}
#endif
