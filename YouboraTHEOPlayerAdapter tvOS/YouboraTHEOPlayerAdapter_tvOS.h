//
//  YouboraTHEOPlayerAdapter_tvOS.h
//  YouboraTHEOPlayerAdapter tvOS
//
//  Created by Enrique Alfonso Burillo on 22/06/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraTHEOPlayerAdapter_tvOS.
FOUNDATION_EXPORT double YouboraTHEOPlayerAdapter_tvOSVersionNumber;

//! Project version string for YouboraTHEOPlayerAdapter_tvOS.
FOUNDATION_EXPORT const unsigned char YouboraTHEOPlayerAdapter_tvOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraTHEOPlayerAdapter_tvOS/PublicHeader.h>
