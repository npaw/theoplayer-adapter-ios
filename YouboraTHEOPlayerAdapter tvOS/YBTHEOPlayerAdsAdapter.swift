//
//  YBTHEOPlayerAdsAdapter.swift
//  YouboraTHEOPlayerAdapter tvOS
//
//  Created by Jorge on 03/10/2018.
//  Copyright © 2018 Jorge Domínguez Martínez. All rights reserved.
//

import YouboraLib
#if canImport(THEOplayerSDK)
import THEOplayerSDK

public class YBTHEOPlayerAdsAdapter: YBPlayerAdapter<AnyObject> {
    fileprivate var adBreakStartListener: EventListener?
    fileprivate var adBreakEndListener: EventListener?
    fileprivate var adLoadedListener: EventListener?
    fileprivate var adBeginListener: EventListener?
    fileprivate var adStopListener: EventListener?
    fileprivate var adErrorListener: EventListener?
    fileprivate var adFirstQuartileListener: EventListener?
    fileprivate var adMidQuartileListener: EventListener?
    fileprivate var adThirdQuartileListener: EventListener?
    fileprivate var adImpressionListener: EventListener?
    
    fileprivate var lastReportedPlayhead: Double?
    fileprivate var listenersRegistered: Bool
    fileprivate var lastKnownAd: Ad?
    fileprivate var lastKnownAdBreak: AdBreak?
    // We must override this init in order to add our init (happens because of interopatability of youbora objc framework with swift).
    private override init() {
        self.listenersRegistered = false
        super.init()
    }
    
    public override init(player: AnyObject) {
        self.listenersRegistered = false
        super.init(player: player)
        registerListeners()
        lastReportedPlayhead = 0
    }
}

extension YBTHEOPlayerAdsAdapter {
    override public func registerListeners() {
        super.registerListeners()
        registerEvents()
    }
    
    override public func unregisterListeners() {
        unregisterEvents()
        super.unregisterListeners()
    }
}

/************************************************************/
// MARK: - Events Handling
/************************************************************/

extension YBTHEOPlayerAdsAdapter {
    fileprivate func registerEvents() {
        if self.listenersRegistered { return }
        if let player = player as? THEOplayer {
            self.listenersRegistered = true
            self.adBreakStartListener = player.ads.addEventListener(type: AdsEventTypes.AD_BREAK_BEGIN, listener: { [weak self] event in
                self?.lastKnownAdBreak = event.ad
                self?.fireAdBreakStart()
            })
            
            self.adBreakEndListener = player.ads.addEventListener(type: AdsEventTypes.AD_BREAK_END, listener: { [weak self] event in
                self?.lastKnownAdBreak = nil
                self?.fireAdBreakStop()
            })
            
            self.adLoadedListener = player.ads.addEventListener(type: AdsEventTypes.AD_LOADED) { event in }
            
            self.adBeginListener = player.ads.addEventListener(type: AdsEventTypes.AD_BEGIN) { [weak self] event in
                self?.lastKnownAd = event.ad
                
                self?.fireStart()
                self?.fireJoin()
            }
            
            self.adStopListener = player.ads.addEventListener(type: AdsEventTypes.AD_END) { [weak self] event in
                self?.fireStop()
                
                self?.lastKnownAd = nil
            }
            
            self.adErrorListener = player.ads.addEventListener(type: AdsEventTypes.AD_ERROR) { [weak self] event in
                self?.fireError(withMessage: event.error, code: event.type, andMetadata: nil)
            }
            
            self.adFirstQuartileListener = player.ads.addEventListener(type: AdsEventTypes.AD_FIRST_QUARTILE) { [weak self] event in
                self?.fireQuartile(1)
            }
            
            self.adMidQuartileListener = player.ads.addEventListener(type: AdsEventTypes.AD_MIDPOINT) { [weak self] event in
                self?.fireQuartile(2)
            }
            
            self.adThirdQuartileListener = player.ads.addEventListener(type: AdsEventTypes.AD_THIRD_QUARTILE) { [weak self] event in
                self?.fireQuartile(3)
            }
            
            self.adImpressionListener = player.ads.addEventListener(type: AdsEventTypes.AD_IMPRESSION) { [weak self] event in
                self?.fireJoin()
            }
        }
    }
    
    fileprivate func unregisterEvents() {
        if let player = player as? THEOplayer {
            if let listener = self.adBreakStartListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_BREAK_BEGIN, listener: listener)
            }
            
            if let listener = self.adBreakEndListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_BREAK_END, listener: listener)
            }
            
            if let listener = self.adLoadedListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_LOADED, listener: listener)
            }
            
            if let listener = self.adBeginListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_BEGIN, listener: listener)
            }
            
            if let listener = self.adStopListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_END, listener: listener)
            }
            
            if let listener = self.adErrorListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_ERROR, listener: listener)
            }
            
            if let listener = self.adFirstQuartileListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_FIRST_QUARTILE, listener: listener)
            }
            
            if let listener = self.adMidQuartileListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_MIDPOINT, listener: listener)
            }
            
            if let listener = self.adThirdQuartileListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_THIRD_QUARTILE, listener: listener)
            }
            
            if let listener = self.adImpressionListener {
                player.ads.removeEventListener(type: AdsEventTypes.AD_IMPRESSION, listener: listener)
            }
            
        }
    }
}

/************************************************************/
// MARK: - Info Methods
/************************************************************/

extension YBTHEOPlayerAdsAdapter {
    override public func getPlayerVersion() -> String? {
        return Utils.getPlayerVersion()
    }
    
    override public func getPlayerName() -> String? {
        return Utils.getPlayerNameWithOs(ads: true)
    }
    
    override public func getVersion() -> String {
        return Utils.getPlayerNameWithOsAndVersion(ads: true)
    }

    public override func getTitle() -> String? {
        guard let title = self.lastKnownAd?.id else {
            return nil
        }
        
        return title
    }
    
    public override func getGivenAds() -> NSNumber? {
        guard let ads = self.lastKnownAdBreak?.ads else {
            return nil
        }
        
        return NSNumber(value: ads.count)
    }
    
    public override func getResource() -> String? {
        guard let resource = self.lastKnownAd?.resourceURI else {
            return nil
        }
        
        return resource
    }
    
    public override func isSkippable() -> NSValue? {
        guard let skipOffset = self.lastKnownAd?.skipOffset else {
            return NSNumber(value: false)
        }
        
        return skipOffset != -1 ? NSNumber(value: true) : NSNumber(value: false)
    }
    
    override public func getPlayhead() -> NSNumber? {
        if let player = player as? THEOplayer {
            let currentTime = player.currentTime
            self.lastReportedPlayhead = currentTime
        }
        return NSNumber(value: lastReportedPlayhead!)
    }
    
    override public func getPlayrate() -> NSNumber {
        if let player = player as? THEOplayer {
            if (player.paused) {
                return 0.0
            } else if player.playbackRate == 0.0 {
                return 1.0
            } else {
                return NSNumber(value: player.playbackRate)
            }
        }
        return super.getPlayrate()
    }
}
#endif
