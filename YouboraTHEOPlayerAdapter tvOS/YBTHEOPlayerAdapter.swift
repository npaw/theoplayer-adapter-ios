//
//  YBTHEOPlayerAdapter.swift
//  YouboraTHEOPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 22/06/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

import YouboraLib
#if canImport(THEOplayerSDK)
import THEOplayerSDK

public class YBTHEOPlayerAdapter: YBPlayerAdapter<AnyObject> {
    
    fileprivate var playEventListener: Any?
    fileprivate var pauseEventListener: Any?
    fileprivate var playingEventListener: Any?
    fileprivate var seekingEventListener: Any?
    fileprivate var seekedEventListener: Any?
    fileprivate var videoEndListener: Any?
    fileprivate var errorListener: Any?
    fileprivate var networkErrorListener: Any?
    fileprivate var sourceChangeEventListener: Any?
    fileprivate var waitingEventListener: Any?
    fileprivate var canPlayEventListener: Any?
    fileprivate var currentTimeListener: Any?
    fileprivate var adBreakBeginEventListener: Any?
    fileprivate var adBreakEndEventListener: Any?
    fileprivate var destroyEventListener: Any?
    
    fileprivate var lastReportedPlayhead: Double = 0
    fileprivate var lastReportedWidth: Int = 0
    fileprivate var lastReportedHeight: Int = 0
    
    fileprivate var isOnAdBreak: Bool = false
    
    fileprivate var lastErrorSent: THEOError?
    
    fileprivate var fatalNetworkErrors = [404, 403]
    
    // We must override this init in order to add our init (happens because of interopatability of youbora objc framework with swift).
    private override init() {
        super.init()
    }
    
    override public init(player: AnyObject) {
        super.init(player: player)
        registerListeners()
        lastReportedPlayhead = 0
        lastReportedWidth = 0
        lastReportedHeight = 0
        isOnAdBreak = false
    }
    
    
    public func appendNetworkFatalErros(networkFatalErros: [Int]) {
        self.fatalNetworkErrors.append(contentsOf: networkFatalErros)
    }
}

extension YBTHEOPlayerAdapter{
    override public func registerListeners() {
        super.registerListeners()
        registerEvents()
    }
    
    override public func unregisterListeners() {
        unregisterEvents()
        if #available(iOS 15.0, *) { // Running iOS 15 or newer
            monitor?.stop()
        }
        super.unregisterListeners()
    }
}

/************************************************************/
// MARK: - Events Handling
/************************************************************/

extension YBTHEOPlayerAdapter {
    private func checkIfEventErrorExists() -> Bool {
        return true
    }
    
    fileprivate func registerEvents() {
        
        if let player = player as? THEOplayer {
            self.playEventListener = player.addEventListener(type: PlayerEventTypes.PLAY, listener: { [weak self] event in
                self?.handlePlayEvent(event: event)
            })
            self.pauseEventListener = player.addEventListener(type: PlayerEventTypes.PLAYING, listener: { [weak self] event in
                self?.handlePlayingEvent(event: event)
            })
            self.playingEventListener = player.addEventListener(type: PlayerEventTypes.PAUSE, listener: { [weak self] event in
                self?.handlePauseEvent(event: event)
            })
            self.seekingEventListener = player.addEventListener(type: PlayerEventTypes.SEEKING, listener: { [weak self] event in
                self?.handleSeekinEvent(event: event)
            })
            self.seekedEventListener = player.addEventListener(type: PlayerEventTypes.SEEKED, listener: { [weak self] event in
                self?.handleSeekedEvent(event: event)
            })
            self.waitingEventListener = player.addEventListener(type: PlayerEventTypes.WAITING, listener: { [weak self] event in
                self?.handleWaitingEvent(event: event)
            })
            self.canPlayEventListener = player.addEventListener(type: PlayerEventTypes.CAN_PLAY_THROUGH, listener: { [weak self] event in
                self?.handleCanPlayEvent(event: event)
            })
            self.currentTimeListener = player.addEventListener(type: PlayerEventTypes.TIME_UPDATE, listener: { [weak self] event in
                self?.onTimeUpdate(event: event)
            })
            self.videoEndListener = player.addEventListener(type: PlayerEventTypes.ENDED, listener: { [weak self] event in
                self?.handleEndedEvent(event: event)
            })
            self.errorListener = player.addEventListener(type: PlayerEventTypes.ERROR, listener: { [weak self] event in
                self?.handleError(event: event)
            })
            self.networkErrorListener = player.network.addEventListener(type: NetworkEventTypes.ERROR, listener: { [weak self] event in
                self?.handleNetworkError(event: event)
            })
            self.sourceChangeEventListener = player.addEventListener(type: PlayerEventTypes.SOURCE_CHANGE, listener: { [weak self] event in
                self?.handleSourceChangeEvent(event: event)
            })
            self.destroyEventListener = player.addEventListener(type: PlayerEventTypes.DESTROY, listener: { [weak self] event in
                self?.handleDestroyEvent(event: event)
            })
            #if os(iOS)
            if self.adBreakBeginEventListener == nil {
                self.adBreakBeginEventListener = player.ads.addEventListener(type: AdsEventTypes.AD_BREAK_BEGIN, listener: { [weak self] event in
                    self?.handleAdBreakBeginEvent(event: event)
                })
            }
            
            self.adBreakEndEventListener = player.ads.addEventListener(type: AdsEventTypes.AD_BREAK_END, listener: { [weak self] event in
                self?.handleAdBreakEndEvent(event: event)
            })
            #endif
        }
        
    }
    
    fileprivate func unregisterEvents() {
        if let player = player as? THEOplayer {
            if let playEventListener = self.playEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.PLAY, listener: playEventListener)
            }
            if let playingEventListener = self.playingEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.PLAYING, listener: playingEventListener)
            }
            if let pauseEventListener = self.pauseEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.PAUSE, listener: pauseEventListener)
            }
            if let seekingEventListener = self.seekingEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.SEEKING, listener: seekingEventListener)
            }
            if let seekedEventListener = self.seekedEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.SEEKED, listener: seekedEventListener)
            }
            
            if let waitingEventListener = self.waitingEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.WAITING, listener: waitingEventListener)
            }
            
            if let canPlayEventListener = self.canPlayEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.CAN_PLAY_THROUGH, listener: canPlayEventListener)
            }
            
            if let sourceChangeEventListener = self.sourceChangeEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.SOURCE_CHANGE, listener: sourceChangeEventListener)
            }
            
            if let errorListener = self.errorListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.ERROR, listener: errorListener)
            }
            
            if let networkErrorListener = self.networkErrorListener as? EventListener {
                player.network.removeEventListener(type: NetworkEventTypes.ERROR, listener: networkErrorListener)
            }
            
            if let videoEndListener = self.videoEndListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.ENDED, listener: videoEndListener)
            }
            
            if let destroyEventListener = self.playEventListener as? EventListener {
                player.removeEventListener(type: PlayerEventTypes.DESTROY, listener: destroyEventListener)
            }
            
            #if os(iOS)
            if let adBreakBeginEventListener = self.adBreakBeginEventListener as? EventListener {
                player.removeEventListener(type: AdsEventTypes.AD_BREAK_BEGIN, listener: adBreakBeginEventListener)
            }
            if let adBreakEndEventListener = self.adBreakEndEventListener as? EventListener {
                player.removeEventListener(type: AdsEventTypes.AD_BREAK_END, listener: adBreakEndEventListener)
            }
            #endif
        }
        
    }
}

/************************************************************/
// MARK: - Events Implementation
/************************************************************/

extension YBTHEOPlayerAdapter {
    func handlePlayEvent(event : PlayEvent) {
        if !isOnAdBreak {
            fireResume()
            fireStart()
        }
    }
    
    func handlePlayingEvent(event : PlayingEvent) {
        fireJoin()
        fireSeekEnd()
        fireBufferEnd()
    }
    
    func handlePauseEvent(event : PauseEvent) {
        if !isOnAdBreak {
            firePause()
        }
    }
    
    func handleSourceChangeEvent(event : SourceChangeEvent) {
        fireStop()
    }
    
    func handleSeekinEvent(event : SeekingEvent) {
        fireSeekBegin()
    }
    
    func handleSeekedEvent(event : SeekedEvent) {
        fireSeekEnd()
    }
    
    func handleWaitingEvent(event : WaitingEvent) {
        fireBufferBegin()
    }

    func handleCanPlayEvent(event : CanPlayThroughEvent) {
        fireBufferEnd()
    }
    
    func onTimeUpdate(event: TimeUpdateEvent) {
        if !flags.seeking && !isOnAdBreak {
            lastReportedPlayhead = event.currentTime
        }
    }
    
    func handleEndedEvent(event: EndedEvent) {
        fireStop()
    }
    
    func handleNetworkError(event: NetworkErrorEvent) {
        guard let error = event.error else {
            fireError(withMessage: event.type, code: nil, andMetadata: nil)
            return
        }
        
        let message = error.errorComment != nil ? error.errorComment : error.message
        
        if fatalNetworkErrors.contains(error.status) {
            fireFatalError(withMessage: message, code: "\(error.code)", andMetadata: nil)
        } else {
            fireError(withMessage: message, code: "\(error.code)", andMetadata: nil)
        }
    }
    
    func handleError(event: ErrorEvent) {
        guard let error = event.errorObject else {
            fireError(withMessage: event.type, code: event.error, andMetadata: nil)
            return
        }
        
        if error.code != .NETWORK_ERROR {
            fireFatalError(withMessage: error.message, code: "\(error.code)", andMetadata: nil)
        }
    }
    
    func handleDestroyEvent(event: DestroyEvent) {
        if plugin?.adsAdapter != nil {
            plugin?.adsAdapter?.fireAdBreakStop()
            plugin?.adsAdapter?.fireStop()
        }
        fireStop()
        player = nil
    }
    
    #if os(iOS)
    func handleAdBreakBeginEvent(event: AdBreakBeginEvent) {
        isOnAdBreak = true
    }
    
    func handleAdBreakEndEvent(event: AdBreakEndEvent) {
        isOnAdBreak = false
    }
    #endif
}

/************************************************************/
// MARK: - Info Methods
/************************************************************/

extension YBTHEOPlayerAdapter {
    
    override public func getDuration() -> NSNumber? {
        if let player = player as? THEOplayer {
            guard let _ = player.duration else {
                return super.getDuration()
            }
            return NSNumber(value: (player.duration)!)
        }
        return super.getDuration()
    }
    
    override public func getResource() -> String? {
        if let player = player as? THEOplayer {
            if let src = player.src {
                return src
            }
        }
        return super.getResource()
    }
    
    override public func getPlayhead() -> NSNumber? {
        return NSNumber(value: lastReportedPlayhead)
    }
    
    override public func getPlayrate() -> NSNumber {
        if let player = player as? THEOplayer {
            if (player.paused) {
                return 0.0
            } else if player.playbackRate == 0.0 {
                return 1.0
            } else {
                return NSNumber(value: player.playbackRate)
            }
        }
        return super.getPlayrate()
    }
    
    override public func getRendition() -> String? {
        if let player = player as? THEOplayer {
            
            if self.lastReportedWidth == 0 && self.lastReportedHeight == 0 {
                self.lastReportedWidth = Int(player.frame.size.width)
                self.lastReportedHeight = Int(player.frame.size.height)
            }
            
            let width = player.videoWidth
            self.lastReportedWidth = width
            
            let height = player.videoHeight
            self.lastReportedHeight = height
        }
        
        return YBYouboraUtils.buildRenditionString(withWidth: Int32(lastReportedWidth), height: Int32(lastReportedHeight), andBitrate: 0)
    }
    
    override public func getPlayerVersion() -> String? {
        return Utils.getPlayerVersion()
    }
    
    override public func getPlayerName() -> String? {
        return Utils.getPlayerNameWithOs(ads: false)
    }
    
    override public func getVersion() -> String {
        return Utils.getPlayerNameWithOsAndVersion(ads: false)
    }
}
#endif
