# YouboraTHEOPlayerAdapter

A framework that will collect several video events from the THEOPlayer and send it to the back end

# Installation

#### CocoaPods

If you use a local framework follow the step 0, otherwise skip to step 1.

0. Create a local podspec which includes the THEOPlayer framework like the following

```bash
Pod::Spec.new do |spec|

  spec.summary = 'To link vendored frameworks to the app (for convenience and architecture stripping scripts).'

  spec.vendored_frameworks = 'Frameworks/THEOplayerSDK.xcframework'

    # Add dependencies if needed
    spec.dependency 'GoogleAds-IMA-iOS-SDK'

    spec.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES' }

    # As the podspec is local, these information is not important
    spec.name = 'THEOplayerSDK'
    spec.version = '0.1.0'
    spec.authors = ''
    spec.homepage = 'http://example.com'
    spec.source = { :http => 'http://example.com' }
end
```

1. Insert into your Podfile

```bash
pod 'YouboraTHEOPlayerAdapter'
```

If using the local podspec also add:

```bash
pod 'THEOplayerSDK', :path => './'
```

2. Add the following pre-install hook

```bash
pre_install do |installer|
  adapter_target_name = "YouboraTHEOPlayerAdapter"
  puts "Patching dependencies of #{adapter_target_name}"
  def target_by_name(installer, name)
    target = installer.pod_targets.filter { |t| t.name == name }.first
    raise "No target '#{name}'" unless target
    target
  end 
  adapter_target = target_by_name(installer, adapter_target_name)
  adapter_target.dependent_targets <<= target_by_name(installer, "THEOplayerSDK")
end
```

3. Execute the command

```bash
pod install
```

#### Carthage

1. Insert into your Cartfile

```bash
git "https://bitbucket.org/npaw/theoplayer-adapter-ios" 
```

2. run this command to get the code without build

```bash
carthage update --no-build
```

3. copy your **TheoPlayerSDK** framework and past it into the path : **ProjectDir/Carthage/Checkouts/theoplayer-adapter-ios**

4. execute the command to compile the library 

```bash
carthage update
```

5. follow the normal steps to include the generated frameworks from Carthage, go to **{YOUR_SCHEME} > Build Settings > Framework Search Paths** and add **\$(PROJECT_DIR)/Carthage/Build/{iOS, Mac, tvOS or the platform of your scheme}**

## How to use

## Start plugin and options

```swift

//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
        let options = YBOptions()
        options.contentResource = "http://example.com"
        options.accountCode = "accountCode"
        options.adResource = "http://example.com"
        options.contentIsLive = NSNumber(value: false)
        return options;
    }
    
lazy var plugin = YBPlugin(options: self.options)
```

### YBTHEOPlayerAdapter

```swift
import YouboraTHEOPlayerAdapter

...

//Once you have your player and plugin initialized you can set the adapter
guard let player = player as? THEOplayer else {
  return
}

self.plugin.adapter = YBTHEOPlayerAdapter(player: player)

...

//If you want to setup the ads adapter as well
guard let player = player as? THEOplayer else {
  return
}

self.plugin.adapter = YBTHEOPlayerAdapter(player: player)

...

// When the view gonna be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
self.plugin.fireStop()
self.plugin.removeAdapter()
self.plugin.removeAdsAdapter()
```

#### Errors handling

In TheoPlayer there are two types of errors. Network and player based errors. All the player errors are fatal, but the network ones sometimes are not fatal. 
By default there are two erros that are fatal **(404, 403)**. But the adapter has a function that allow you to add other codes. For that you should follow the example: 

```swift
adapter.appendNetworkFatalErros(networkFatalErros: [405, 406])
```
## Run samples project

1. Run the command 
```bash
pod install
```

2. Copy the THEOPlayer framework to a folder with the follow struct
```
project
└───Frameworks
│   └───iOS
│   └───tvOS
```
You don't have to copy tvOS as well just if you wanna run it.

The project should be ready now to work

---
**NOTES**

**To check the adapter and player compatibilities please check the CHANGELOG file**

If you wanna generate the adapter framework with Carthage your should comment the pod 'google-cast-sdk' and run pod install again.
After that you should download [GoogleCast framework](https://developers.google.com/cast/docs/ios_sender) and past it on the folder 'Frameworks/iOS or tvOS' that you created to run the samples project.


Case you have problems with Swift headers not found, when you're trying to install via **carthage** please do the follow instructions: 

 1. Remove Carthage folder from the project
 2. Execute the follow command ```rm -rf ~/Library/Caches/org.carthage.CarthageKit```

---