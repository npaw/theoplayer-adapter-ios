//
//  ViewController.swift
//  THEOPlayerExampletvOS
//
//  Created by Enrique Alfonso Burillo on 21/06/2018.
//  Copyright © 2018 Enrique Alfonso Burillo. All rights reserved.
//

import UIKit
import THEOplayerSDK
import GameController
import YouboraTHEOPlayerAdapter
import YouboraLib

class ViewController: UIViewController {

    var player: THEOplayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        YBLog.setDebugLevel(YBLogLevel.verbose)
        player = THEOplayer()
        player?.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        player?.addAsSubview(of: self.view)
        let source = SourceDescription(source: TypedSource(src: "https://cdn.theoplayer.com/video/elephants-dream/playlist.m3u8", type: "application/x-mpegurl"))
        player?.source = source
        
        let options = YBOptions.init()
        options.accountCode = "powerdev"
        options.httpSecure = false
        options.autoDetectBackground = true
        let plugin = YBPlugin(options: options)
        plugin.adapter = YBTHEOPlayerAdapter(player: player!)
        player?.play()
        
        addGestureRecognizerWithType(pressType: UIPress.PressType.playPause, selector: #selector(ViewController.playPause));
        addGestureRecognizerWithType(pressType: UIPress.PressType.rightArrow, selector: #selector(ViewController.rightArrow));
        addGestureRecognizerWithType(pressType: UIPress.PressType.leftArrow, selector: #selector(ViewController.leftArrow));
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ViewController.userPanned))
        self.view.addGestureRecognizer(panGestureRecognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func handlePlayEvent(event : PlayEvent) {
        print("Received \(event.type) event at \(event.currentTime)")
    }
    
    func addGestureRecognizerWithType(pressType : UIPress.PressType, selector : Selector) {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
        tapGestureRecognizer.allowedPressTypes = [NSNumber.init(value: pressType.rawValue)]
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func playPause(){
        guard let _ = self.player else{
            return
        }
        if let player = self.player {
            if(player.paused){
                player.play()
            }else{
                player.pause()
            }
        }
    }
    
    @objc func rightArrow(){
        guard let _ = self.player else{
            return
        }
        if let player = self.player {
            player.requestCurrentTime(completionHandler: {(currentTime,error) in
                player.setCurrentTime(currentTime! + 30)
            });
        }
    }
    
    @objc func leftArrow(){
        guard let _ = self.player else{
            return
        }
        if let player = self.player {
            player.requestCurrentTime(completionHandler: {(currentTime,error) in
                player.setCurrentTime(currentTime! - 30)
            });
        }
    }
    
    @IBAction func userPanned(_ panGestureRecognizer : UIPanGestureRecognizer){
        NSLog("PAN")
    }
}

